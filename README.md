# Yet Another Thesis Template

Version: 0.1  
Copyright (C) 2014-2016 [Nicolas Navarro-Guerrero](https://nicolas-navarro-guerrero.github.io)  
Contact:  nicolas.navarro.guerrero@gmail.com  

        https://github.com/nicolas-navarro-guerrero
        https://bitbucket.org/nicolas-navarro-guerrero
        https://nicolas-navarro-guerrero.github.io

*******************************************************************************
## Overview

*******************************************************************************
### File structure:

*******************************************************************************
### Usage
* Main file `thesis.tex`

* Complete the file `metadata.tex` with your personal information

* A simple versioning mark is included in the footer, it comes handy during document production. Once you have the final version, comment or delete the command
`\def\thesisdraft{1}` at the beginning of file `thesis.tex`

* To use reference list grouped alphabetically, you have to add the initial of author's surname into the `keywords` field of each bibtex entry

* the fancy ToDo list can be disabled on the final version by simple uncommenting `disable` in the declaration on the `todonotes` package at the end of file `document-formatting.tex`

#### Configuring [Texmaker](http://www.xm1math.net/texmaker/)  
Go to `options` -> `Configure Texmaker` -> `commands`  

* Check `use a build subdirectory for output files`  
    
* change from `bibtex %` -> `biber build/%`  

* change from `makeindex %.idx` -> `makeindex build/%.idx`  

Go to `user` -> `user commands` -> `edit user commands` (optional)  

* add `makeglossaries -d build %`  

Compile using pdflatex, bibtex, makeindex (optional), makeglossaries (optional)

*******************************************************************************
### Suggested packages included in the documents preamble (`document-formatting.tex`):
Tested on Ubuntu `14.04`, biber `1.8.1`, texlive `2013.20140215`  
List of the packages included and a link to their documentation:  

#### Font, Language and Dates  
* [inputenc](http://www.ctan.org/tex-archive/macros/latex/base) utf8  
* [fontenc](http://www.ctan.org/pkg/fontenc) T1  
* [lmodern](http://www.ctan.org/tex-archive/fonts/lm)  
* [babel](http://www.ctan.org/tex-archive/macros/latex/required/babel/base)  
* [csquotes](http://www.ctan.org/tex-archive/macros/latex/contrib/csquotes)  
* [datetime](http://www.ctan.org/tex-archive/macros/latex/contrib/datetime)  

#### Document Formatting and Style
* [microtype](http://www.ctan.org/tex-archive/macros/latex/contrib/microtype)  
* [lettrine](http://www.ctan.org/tex-archive/macros/latex/contrib/lettrine)  
* [titlesec](http://www.ctan.org/pkg/titlesec)  
* [setspace](http://www.ctan.org/tex-archive/macros/latex/contrib/setspace)  
* [fancyhdr](http://www.ctan.org/tex-archive/macros/latex/contrib/fancyhdr)  
* [layout](http://www.ctan.org/tex-archive/macros/latex/required/tools)  
* [geometry](http://www.ctan.org/pkg/geometry)  
* [parskip](http://www.ctan.org/pkg/parskip)  
* [multicol](http://www.ctan.org/pkg/multicol)  

#### Mathematics
* [amsmath](http://www.ctan.org/tex-archive/macros/latex/required/amslatex/math)  
* [amstext](http://www.ctan.org/tex-archive/macros/latex/required/amslatex/math)  
* amssymb  
* [amsthm](http://www.ctan.org/tex-archive/macros/latex/required/amslatex/amscls)  
* [amsfonts](http://www.ctan.org/tex-archive/fonts/amsfonts)  
* [xfrac](http://www.ctan.org/tex-archive/macros/latex/contrib/l3packages)  
* [array](http://www.ctan.org/tex-archive/macros/latex/required/tools)
* [cancel](http://ctan.org/tex-archive/macros/latex/contrib/cancel)  

#### Tables
* [multirow](http://www.ctan.org/tex-archive/macros/latex/contrib/multirow)  

#### Figures, Images
* [picture](http://www.ctan.org/pkg/picture)  
* [tikz](http://www.ctan.org/tex-archive/macros/latex/contrib/tkz/tkz-base)  
* [graphicx](http://www.ctan.org/tex-archive/macros/latex/required/graphics)  
* [float](http://www.ctan.org/tex-archive/macros/latex/contrib/float)  
* [srcltx](http://www.ctan.org/tex-archive/macros/latex/contrib/srcltx)  
* [transparent](http://www.ctan.org/pkg/transparent)  
* [calc](http://www.ctan.org/tex-archive/macros/latex/required/tools)  
* [subfig](http://www.ctan.org/tex-archive/macros/latex/contrib/subfig)  
* [caption](http://www.ctan.org/tex-archive/macros/latex/contrib/caption)  
* [subcaption](http://www.ctan.org/tex-archive/macros/latex/contrib/caption)  
* [verbatim](http://www.ctan.org/tex-archive/macros/latex/required/tools)  

#### Algorithms and Pseudocode
* [algpseudocode](http://www.ctan.org/tex-archive/macros/latex/contrib/algorithmicx)  
* [algorithm](http://www.ctan.org/tex-archive/macros/latex/contrib/algorithmicx)  

#### Bibliography
* [biblatex](http://www.ctan.org/tex-archive/macros/latex/contrib/biblatex)  
* [xpatch](http://www.ctan.org/tex-archive/macros/latex/contrib/xpatch)  

#### Others
* [url](http://www.ctan.org/tex-archive/macros/latex/contrib/url)  
* [hyperref](http://www.ctan.org/tex-archive/macros/latex/contrib/hyperref)  
* [xcolor](http://www.ctan.org/tex-archive/macros/latex/contrib/xcolor)  
* [comment](http://www.ctan.org/tex-archive/macros/latex/contrib/comment)  
* [todonotes](http://ctan.org/tex-archive/macros/latex/contrib/todonotes)  
* [siunitx](http://ctan.org/tex-archive/macros/latex/contrib/siunitx)
* [makeidx](http://www.ctan.org/pkg/makeidx)  
* [glossaries](http://www.ctan.org/tex-archive/macros/latex/contrib/glossaries)  

*******************************************************************************
## LICENSE FOR DOCUMENTATION AND FIGURES:
The collection of source files and the images generated directly from them are work by Nicolás Navarro-Guerrero and are licensed under the Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

*******************************************************************************
## LICENSE FOR TEX FILE:
This collection of scripts, tex and source files are part of 
"yet another thesis template" 
and are free software: you can redistribute it and/or modify it under the terms 
of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version. 

"yet another thesis template" 
is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with 
"yet another thesis template" 
If not, see https://www.gnu.org/licenses/. 
